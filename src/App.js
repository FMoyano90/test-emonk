import React, { useState } from "react";
import Swal from "sweetalert2";
import "./App.css";

function App() {
  const [mostrar, setMostrar] = useState(true);

  const [mensaje, setMensaje] = useState("");

  const [inputValue, setInputValue] = useState({
    name: "",
    password: "",
  });

  const [cartaCliente, setCartaCliente] = useState(null);

  const [cartaBanca, setCartaBanca] = useState(null);

  const { name, password } = inputValue;

  const usuario = {
    name: "emonk",
    password: "emonkrocks",
  };

  const actualizarState = (e) => {
    setInputValue({
      ...inputValue,
      [e.target.name]: e.target.value,
    });
  };

  const login = (e) => {
    e.preventDefault();
    if (
      inputValue.name === usuario.name &&
      inputValue.password === usuario.password
    ) {
      setMostrar(false);
      Swal.fire({
        icon: "success",
        title: "Bienvenido",
        text: "¡Ingresa a jugar!",
      });
    } else {
      setMostrar(true);
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Datos incorrectos",
      });
    }
  };

  const actualizarStateCarta = (e) => {
    if (e.target.value >= 1 && e.target.value <= 10) {
      setCartaCliente(e.target.value);
      setMensaje("");
    } else {
      setMensaje("Escoge un número valido");
    }
  };

  const repartirCartas = () => {
    if (cartaCliente) {
      let valor = Math.floor(Math.random() * 9 + 1);
      setMensaje("Sacando carta de la banca");
      setTimeout(() => {
        setCartaBanca(valor);
        setTimeout(() => {
          setMensaje("Verificando ganador");
          verificarGanador();
        }, 1000);
      }, 1500);
    } else {
      setMensaje("Escogé un número primero");
    }
  };

  const verificarGanador = () => {
    // Si la banca obtiene par
    if (cartaBanca % 2 === 0) {
      if (cartaBanca < cartaCliente) {
        return setMensaje(
          "Más suerte para la próxima. Tu contrincante es el ganador"
        );
      } else if (cartaBanca === cartaCliente) {
        return setMensaje("Empate");
      } else {
        return setMensaje("¡Ganaste esta ronda!");
      }
    }

    // Si la banca obtiene impar
    if (cartaBanca % 2 === 1) {
      if (cartaBanca > cartaCliente) {
        return setMensaje("¡Ganaste esta ronda!");
      } else if (cartaBanca === cartaCliente) {
        return setMensaje("Empate");
      } else {
        return setMensaje(
          "Más suerte para la próxima. Tu contrincante es el ganador"
        );
      }
    }
  };

  const reiniciar = () => {
    setCartaCliente(null);
    setCartaBanca(null);
    setMensaje("");
  };

  return (
    <>
      {mostrar ? (
        <div className="row">
          <div className="col-4 offset-4 mt-5 text-center">
            <h2>INGRESA AL JUEGO</h2>
            <form onSubmit={login} id="formulario">
              <input
                type="text"
                name="name"
                placeholder="Nombre de usuario"
                onChange={actualizarState}
                value={name}
                className="form-control"
              />
              <input
                type="text"
                name="password"
                placeholder="Contraseña"
                onChange={actualizarState}
                value={password}
                className="form-control"
              />
              <button className="btn btn-primary btn-block mt-2" type="submit">
                Ingresar
              </button>
            </form>
          </div>
        </div>
      ) : (
        <div className="row container">
          <div className="col-12">
            <h2>Reglas del juego</h2>
            <p>- El jugador podrá escoger un número entre el 1 y el 10.</p>
            <p>
              - Si el número elegido para el contrincante corresponde a un
              número par, gana el jugador que tenga un número más alto.
            </p>
            <p>
              - Si el número elegido para el contrincante es un número impar,
              gana el jugador que tenga un número más bajo.
            </p>
          </div>
          <div className="col-3">
            <p>
              <b>Escoge tú número:</b>
            </p>
            <input
              type="number"
              name="valor"
              className="form-control"
              onChange={actualizarStateCarta}
            />
            <button className="btn btn-danger" onClick={repartirCartas}>
              Comenzar Juego
            </button>
          </div>
          <div className="col-12 mt-5">
            {cartaCliente ? (
              <p>
                <b>Tú carta:</b> {cartaCliente}
              </p>
            ) : null}
            {cartaBanca ? (
              <p>
                <b>Banca:</b> {cartaBanca}
              </p>
            ) : null}

            <p class="mensaje">{mensaje}</p>
            {cartaCliente && cartaBanca ? (
              <button className="btn btn-warning" onClick={reiniciar}>
                Reiniciar Juego
              </button>
            ) : null}
          </div>
        </div>
      )}
    </>
  );
}

export default App;
